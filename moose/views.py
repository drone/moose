from functools import wraps
from http import HTTPStatus

import peewee
from coq.flask_coq import auth_required
from flask import jsonify, request

from .app import Device, abort, app, logger
from .auth import fc, get_current_user


def json_body_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            data = dict(request.json or None)
        except (TypeError, ValueError):
            abort(400, description='No json body found')
        return func(data, *args, **kwargs)
    return wrapper


@app.route('/device/<registration>', methods=['HEAD'])
def head_device(registration):
    exists = (Device.select().where(Device.registration == registration)
                    .exists())
    return '', exists and HTTPStatus.NO_CONTENT or HTTPStatus.NOT_FOUND


@app.route('/device/<registration>')
# @auth_required(fc)  # Deactivated for demo purpose, TODO: reactivate.
def get_device(registration):
    try:
        device = Device.get(Device.registration == registration)
    except Device.DoesNotExist:
        abort(404, description='Wrong registration number')
    return jsonify(device.as_dict)


@app.route('/device/<registration>', methods=['PUT'])
@auth_required(fc)
@json_body_required
def put_device(data, registration):
    logger.debug('PUT with %s for registration %s', data, registration)
    data['has_camera'] = bool(data.get('has_camera', False))
    # Make sure registration was not also given in the body, otherwise create
    # will complain that we are passing twice the key.
    data.pop('registration', None)
    data['owner'] = get_current_user()
    try:
        Device.create(registration=registration, **data)
    except (peewee.IntegrityError, ValueError) as e:
        abort(400, description='Invalid data {}'.format(e))
    return '', 201


@app.route('/device/<registration>', methods=['PATCH'])
@auth_required(fc)
@json_body_required
def patch_device(data, registration):
    logger.debug('PATCH with %s for registration %s', data,
                 registration)
    try:
        device = Device.get(Device.registration == registration,
                            Device.owner == get_current_user())
    except Device.DoesNotExist:
        abort(404, description='Device with registration `{}` does not '
                               'exist'.format(registration))
    # TODO proper schema validation.
    if 'registration' in data:
        abort(400, description='`registration` should not be passed')
    if 'owner' in data:
        abort(400, description='`owner` should not be passed')

    for key, value in data.items():
        setattr(device, key, value)
    try:
        device.save()
    except peewee.IntegrityError as e:
        abort(400, description='Missing data {}'.format(e))
    return '', 204


@app.route('/device/<registration>', methods=['DELETE'])
@auth_required(fc)
def delete_device(registration):
    logger.debug('DELETE device %s', registration)
    try:
        device = Device.get(Device.registration == registration,
                            Device.owner == get_current_user())
    except Device.DoesNotExist:
        abort(404, description='Device with registration `{}` does not '
                               'exist'.format(registration))
    device.delete_instance()
    return '', 204


@app.route('/device')
@auth_required(fc)
def get_device_list():
    devices = Device.select().filter(Device.owner == get_current_user())
    return jsonify({'data': [device.as_dict for device in devices]})
