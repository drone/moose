import json

import pytest
from flask import session, url_for, g
from flask.testing import FlaskClient

from moose.app import app as myapp
from moose.app import Device, Owner


class Client(FlaskClient):

    def open(self, *args, **kwargs):
        if 'content_type' not in kwargs:
            kwargs['content_type'] = 'application/json'
        if kwargs.get('content_type') == 'application/json':
            if 'data' in kwargs:
                kwargs['data'] = json.dumps(kwargs['data'])
        return super().open(*args, **kwargs)


myapp.test_client_class = Client
old_preprocess_request = myapp.preprocess_request
old_finalize_request = myapp.finalize_request


@pytest.fixture
def app():

    def preprocess_request():
        # Poor man session patching.
        # https://github.com/pytest-dev/pytest-flask/issues/69
        if hasattr(myapp, 'user_id'):
            session['user_id'] = myapp.user_id
            g.user_details = myapp.user_details
        return old_preprocess_request()

    def finalize_request(rv):
        if hasattr(myapp, 'user_id'):
            del myapp.user_details
            del g.user_details
            del myapp.user_id
        return old_finalize_request(rv)

    myapp.preprocess_request = preprocess_request
    myapp.finalize_request = finalize_request
    return myapp


@pytest.fixture
def owner(app):
    owner = Owner.create(franceconnect_id='1234', fullname='Testy Tester',
                         username='tester')
    app.user_id = owner.id
    app.user_details = {'sub': owner.franceconnect_id}
    yield owner
    owner.delete_instance()


@pytest.fixture
def owner2():
    owner2 = Owner.create(franceconnect_id='5678', fullname='Testy Two',
                          username='tester2')
    yield owner2
    owner2.delete_instance()


@pytest.fixture
def device():

    def factory(**kwargs):
        data = {
            'name': 'bar',
            'manufacturer': 'test',
            'model': 'test',
            'has_camera': True,
            'weight': 800,
            'registration': 'ABCDEF123456789012FR',
        }
        data.update(**kwargs)
        return Device.create(**data)
    return factory


def test_device_has_camera_should_default_to_false(owner):
    device = Device.create(name="test", manufacturer="test", model="test",
                           owner=owner, weight=800,
                           registration='ABCDEF123456789012FR')
    assert device.has_camera is False


def test_get_user_should_return_a_user(owner, client):
    response = client.get(url_for('get_user', id=owner.id))
    assert response.json == owner.as_dict


def test_get_user_with_wrong_id_should_return_a_404(owner, client):
    response = client.get(url_for('get_user', id=888))
    assert response.status_code == 404
    assert response.json == {'description': 'Wrong user id'}


def test_get_device_should_return_a_device(device, client, owner):
    drone = device(owner=owner)
    response = client.get(
        url_for('get_device', registration=drone.registration))
    assert response.json == drone.as_dict


def test_get_device_with_wrong_registration_should_return_a_404(owner, client):
    response = client.get(url_for('get_device',
                                  registration='ABCDEF123456789012FR'))
    assert response.status_code == 404
    assert response.json == {'description': 'Wrong registration number'}


def test_head_device_should_return_a_device(device, client, owner):
    drone = device(owner=owner)
    response = client.head(
        url_for('head_device', registration=drone.registration))
    assert response.status_code == 204


def test_head_device_with_unknown_registration_should_return_a_404(owner,
                                                                   client):
    response = client.head(url_for('head_device',
                                   registration='ABCDEF123456789012FR'))
    assert response.status_code == 404


def test_register_put_not_logged_in(client):
    response = client.put(url_for('put_device',
                                  registration='ABCDEF123456789012FR'),
                          data={'name': 'foo'})
    assert response.status_code == 403


def test_register_put_without_data(client, owner):
    response = client.put(url_for('put_device',
                                  registration='ABCDEF123456789012FR'))
    # FIXME this 400 is sent by Flask when doing request.json in the view,
    # so its content is not json.
    assert response.status_code == 400


def test_register_put_with_wrong_content_type(client, owner):
    registration = 'ABCDEF123456789012FR'
    data = {
        'name': 'foo',
        'owner': owner.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1
    }
    response = client.put(url_for('put_device', registration=registration),
                          data=data, content_type='text/html')
    assert response.status_code == 400
    assert response.json['description'] == 'No json body found'


def test_register_put_not_enough_data(client, owner):
    response = client.put(url_for('put_device',
                                  registration='ABCDEF123456789012FR'),
                          data={'name': 'foo'})
    assert response.status_code == 400


def test_register_put_with_invalid_registration_format(client, owner):
    assert not Device.select()
    response = client.put(url_for('put_device', registration='12345'), data={
        'name': 'foo',
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
    })
    assert response.status_code == 400


def test_register_put_with_data(client, owner):
    assert not Device.select()
    registration = 'ABCDEF123456789012FR'
    data = {
        'name': 'foo',
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
    }
    response = client.put(url_for('put_device', registration=registration),
                          data=data)
    assert response.status_code == 201
    assert Device.select()
    device = Device.select().first()
    assert device.registration == registration
    assert device.owner == owner


def test_register_put_with_data_and_registration(client, owner):
    registration = 'ABCDEF123456789012FR'
    data = {
        'name': 'foo',
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1,
        'registration': registration,
    }
    response = client.put(url_for('put_device', registration=registration),
                          data=data)
    assert response.status_code == 201


def test_register_put_without_has_camera(client, owner):
    assert not Device.select()
    registration = 'ABCDEF123456789012FR'
    data = {
        'name': 'foo',
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': None,
    }
    response = client.put(url_for('put_device', registration=registration),
                          data=data)
    assert response.status_code == 201
    assert Device.get().has_camera is False


def test_put_device_with_invalid_json(client, device, owner):
    drone = device(owner=owner, name='old_name')
    response = client.put(url_for('put_device',
                                  registration=drone.registration),
                          data='foo"')
    assert response.status_code == 400


def test_put_device_with_non_dict_json(client, device, owner):
    drone = device(owner=owner, name='old_name')
    response = client.put(url_for('put_device',
                                  registration=drone.registration),
                          data='["foo"]')
    assert response.status_code == 400


def test_register_patch_with_unknown_id_should_be_404(client, owner):
    registration = 'ABCDEF123456789012FR'
    response = client.patch(url_for('patch_device', registration=registration),
                            data={'name': 'new'})
    assert response.status_code == 404
    assert response.json['description'] == \
        'Device with registration `ABCDEF123456789012FR` does not exist'


def test_patch_device(client, device, owner):
    drone = device(owner=owner, name='old_name')
    response = client.patch(url_for('patch_device',
                                    registration=drone.registration),
                            data={'name': 'new_name'})
    assert response.status_code == 204
    assert Device.get(Device.id == drone.id).name == 'new_name'


def test_patch_device_with_invalid_json(client, device, owner):
    drone = device(owner=owner, name='old_name')
    response = client.patch(url_for('patch_device',
                                    registration=drone.registration),
                            data='foo"')
    assert response.status_code == 400


def test_patch_device_with_non_dict_json(client, device, owner):
    drone = device(owner=owner, name='old_name')
    response = client.patch(url_for('patch_device',
                                    registration=drone.registration),
                            data='["foo"]')
    assert response.status_code == 400


def test_delete_device_with_unknown_id_should_be_404(client, device, owner):
    response = client.delete(url_for('delete_device',
                                     registration='ABCDEF123456789012FR'))
    assert response.status_code == 404


def test_delete_device_without_being_logged_in_should_be_403(client, device,
                                                             owner2):
    drone = device(owner=owner2)
    response = client.delete(url_for('delete_device',
                                     registration=drone.registration))
    assert response.status_code == 403


def test_delete_device(client, device, owner):
    drone = device(owner=owner)
    response = client.delete(url_for('delete_device',
                                     registration=drone.registration))
    assert response.status_code == 204
    assert Device.select(Device.id == drone.id).count() == 0


def test_cannot_patch_registration(client, device, owner):
    drone = device(owner=owner)
    response = client.patch(url_for('patch_device',
                                    registration=drone.registration),
                            data={'registration': 'xxxxx'})
    assert response.status_code == 400
    assert Device.get(Device.id == drone.id).registration == drone.registration


def test_mydevices(client, device, owner2, owner):
    device(owner=owner, name='foo1', registration='ABCDEF123456789012FR')
    device(owner=owner2, registration='ABCDEF123456789013FR')
    response = client.get(url_for('get_device_list'))
    assert response.status_code == 200
    assert len(response.json['data']) == 1  # Only MY devices.
    assert response.json['data'][0]['name'] == 'foo1'
