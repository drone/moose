# Users registration

## Retrieve a given user

Perform a `GET` request against
`https://drone.api.gouv.fr/account/user/{user_id}`.

```json
{
  "id": 2,
  "fullname": "As returned by FranceConnect",
  "username": "As returned by FranceConnect (optional)"
}
```
