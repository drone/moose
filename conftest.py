import os

from click.testing import CliRunner

# Do not import from moose at top level, to allow patching DB config.


def pytest_configure(config):
    os.environ['DB_PATH'] = ':memory:'
    from moose.app import initdb
    runner = CliRunner()
    runner.invoke(initdb)


def pytest_runtest_teardown():
    from moose.app import Device, Owner
    Device.delete().execute()
    Owner.delete().where(Owner.username == 'demo2').execute()
