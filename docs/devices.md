# Devices registration

## Register a new device

Send a JSON encoded `PUT` request against
`https://drone.api.gouv.fr/account/device/{registration}`:

```json
{
    "manufacturer": "DJI",
    "name": "My first drone",
    "has_camera": true,
    "model": "Phantom",
    "weight": "900"
}
```

It will return a `201` status code on success.

## Retrieve a particular device

Perform a `GET` request against
`https://drone.api.gouv.fr/account/device/{registration}`.

It will return all data related to a given device:

```json
{
    "registration": "ABCDEF123456789012FR",
    "manufacturer": "DJI",
    "owner": 2,
    "name": "My first drone",
    "has_camera": true,
    "model": "Phantom",
    "weight": "900"
}
```


## Check existence of a particular device

Perform a `HEAD` request against
`https://drone.api.gouv.fr/account/device/{registration}`.

It will only return the appropriated status code:

* either 204 if the device is found
* or 404 if the device is unknown


## Retrieve all devices (for a given user)

### GET

`https://drone.api.gouv.fr/account/device`

```json
{
  "data": [
    {
        "registration": "ABCDEF123456789012FR",
        "manufacturer": "DJI",
        "owner": 2,
        "name": "My first drone",
        "has_camera": true,
        "model": "Phantom",
        "weight": "900"
    }
  ]
}
```


## Update a device

Perform a `PATCH` request against
`https://drone.api.gouv.fr/account/device/{registration}`.

It should return a `204` or a descriptive error.


## Delete a device

Perform a `DELETE` request against
`https://drone.api.gouv.fr/account/device/{registration}`.

It should return a `204` or a descriptive error.
