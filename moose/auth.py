from coq.flask_coq import FranceConnectFlask, auth_required
from flask import jsonify, redirect, request, session, url_for

from .app import Owner, abort, app, get_env

app.config.update(
    FRANCECONNECT_CONSUMER_KEY=get_env('FRANCECONNECT_CONSUMER_KEY', ''),
    FRANCECONNECT_CONSUMER_SECRET=get_env('FRANCECONNECT_CONSUMER_SECRET', '')
)

fc = FranceConnectFlask(
    client_id=app.config['FRANCECONNECT_CONSUMER_KEY'],
    client_secret=app.config['FRANCECONNECT_CONSUMER_SECRET'],
    connected_url=lambda: url_for('authorize', _external=True),
    disconnected_url=lambda: url_for('disconnected', _external=True),
    scope='openid preferred_username given_name family_name'
)


app.route('/login/')(fc.login_view)


@app.route('/logout/')
def logout():
    response = fc.logout_view()
    del session['user_id']
    return response


@app.route('/authorize/')
def authorize():
    response = fc.authorize_view()
    user_id = fc.get_user_id()
    if user_id:
        try:
            owner = Owner.get(Owner.franceconnect_id == user_id)
        except Owner.DoesNotExist:
            user_details = fc.get_user_details()
            owner = Owner.create(
                franceconnect_id=user_id,
                username=user_details.get('preferred_username'),
                fullname='{} {}'.format(user_details.get('given_name'),
                                        user_details.get('family_name'))
            )
    session['user_id'] = owner.id
    return response


@app.route('/disconnected/')
def disconnected():
    return redirect(request.args.get('next'))


@app.route('/me/')
@auth_required(fc)
def user_details():
    user = get_current_user()
    if user:
        return jsonify(user.as_dict)
    abort(403)


@app.route('/user/<int:id>')
# @auth_required(fc)  # Deactivated for demo purpose, TODO: reactivate.
def get_user(id):
    try:
        owner = Owner.get(Owner.id == id)
    except Owner.DoesNotExist:
        abort(404, description='Wrong user id')
    return jsonify(owner.as_dict)


def get_current_user():
    user_id = session.get('user_id')
    if user_id:
        try:
            return Owner.get(Owner.id == user_id)
        except Owner.DoesNotExist:
            return None
